# Telegraph app

Purpose: create an app that convert a message into morse signal...  
This app was generated with `create-react-app`

## Install

- git clone and cd in project folder
- run `npm install`
- install json server globally in your system `npm install -g json-server`
- run your backend `json-server --watch db/db.json -p 3001`
- run the frontend `npm start`

## Unit test

Only App / Morsify and ListMessage have been tested.
