import React, { Component } from "react";
import config from "./../config";
import { http } from "../helpers/Http";
import PubSub from "pubsub-js";
import ListMessageWithLoading from "./ListMessage";

class MessageContainer extends Component {
  pubSubSubscription;
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isLoading: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchMessages();
    this.pubSubSubscription = PubSub.subscribe(
      config.pubSubChannel,
      (msg, data) => {
        if (data && data === "refresh-messages") {
          this.fetchMessages();
        }
      }
    );
  }

  fetchMessages() {
    this.setState({
      isLoading: true
    });

    http.get(`/messages`, response => {
      if (this._isMounted) {
        this.setState({
          messages: response.data.reverse(),
          isLoading: false
        });
      }
    });
  }

  render() {
    const { isLoading, messages } = this.state;
    return <ListMessageWithLoading loading={isLoading} messages={messages} />;
  }

  componentWillUnmount() {
    PubSub.unsubscribe(this.pubSubSubscription);
    this._isMounted = false;
  }
}

MessageContainer.propTypes = {};

export default MessageContainer;
