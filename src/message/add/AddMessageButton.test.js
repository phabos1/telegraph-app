import React from "react";
import { shallow, mount } from "enzyme";
import AddMessageButton from "./AddMessageButton";

it("should render AddMessageButton component", () => {
  const callbackFunction = () => "toggled";
  let wrapper = mount(<AddMessageButton toggleForm={callbackFunction} />);
  expect(wrapper.props().toggleForm).toEqual(callbackFunction);
  expect(wrapper.props().toggleForm()).toEqual("toggled");
});

it("should trigger toggleForm function on click", () => {
  let callbackFunction = jest.fn();
  let wrapper = mount(<AddMessageButton toggleForm={callbackFunction} />);

  wrapper.find("a").simulate("click");
  expect(callbackFunction).toBeCalled();
});
