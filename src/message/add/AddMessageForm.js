import React, { Component } from "react";
import config from "./../../config";
import { MessageStatuses } from "./../ModelMessage";
import { isEmpty } from "lodash";
import { randomId } from "../../helpers/RandomId";
import PubSub from "pubsub-js";
import { http } from "./../../helpers/Http";
import toastr from "toastr";
import "toastr/build/toastr.min.css";
import moment from "moment";
import { morsify, playMorse } from "./../../helpers/Morsify";
import PropTypes from "prop-types";

class AddMessageForm extends Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      message: "",
      author: "",
      isVisible: true,
      isFormValid: false
    };
    this.state = this.defaultState;
  }

  handleChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;

    this.setState(
      {
        [name]: value
      },
      () => {
        this.validateForm();
      }
    );
  };

  validateForm() {
    if (isEmpty(this.state.message) || isEmpty(this.state.author)) {
      return this.setState({
        isFormValid: false
      });
    }
    this.setState({
      isFormValid: true
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    const payload = {
      id: randomId(),
      creationDate: moment().format(),
      content: this.state.message,
      telegram: morsify(this.state.message),
      author: this.state.author,
      status: this.state.isVisible
        ? MessageStatuses.VISIBLE
        : MessageStatuses.PRIVATE
    };

    http.post(`/messages`, payload, message => {
      this.resetForm();
      this.props.toggleForm();
      PubSub.publish(config.pubSubChannel, `refresh-messages`);
      if (message.data.status === MessageStatuses.VISIBLE) {
        playMorse(message.data.telegram);
      }
      toastr.success(`Telegram sent`);
    });
  };

  resetForm = () => {
    this.setState(this.defaultState);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>Name *</label>
          <input
            autoComplete="off"
            type="text"
            name="author"
            placeholder="Your name ?"
            value={this.state.author}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>Message *</label>
          <textarea
            type="text"
            name="message"
            placeholder="Enter some text right here"
            value={this.state.message}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <input
            name="isVisible"
            type="checkbox"
            checked={this.state.isVisible}
            onChange={this.handleChange}
            id="is-visible"
          />
          <label htmlFor="is-visible" className="side-label">
            Visible ?
          </label>
        </div>
        <div className="form-group">
          <button disabled={!this.state.isFormValid} type="submit">
            Send telegram
          </button>
        </div>
      </form>
    );
  }
}

AddMessageForm.propTypes = {
  toggleForm: PropTypes.func
};

export default AddMessageForm;
