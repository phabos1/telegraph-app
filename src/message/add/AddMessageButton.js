import React from "react";
import telegraph from "./../../assets/img/telegraph.png";
import PropTypes from "prop-types";

const AddMessageButton = props => {
  return (
    <div className="add-message right">
      <a href="#" onClick={props.toggleForm}>
        <img src={telegraph} alt="add message" />
      </a>
    </div>
  );
};

AddMessageButton.propTypes = {
  toggleForm: PropTypes.func
};

export default AddMessageButton;
