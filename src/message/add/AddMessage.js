import React, { Component } from "react";
import AddMessageForm from "./AddMessageForm";
import AddMessageButton from "./AddMessageButton";

class AddMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayForm: false
    };
  }

  toggleForm = () => {
    this.setState({
      displayForm: !this.state.displayForm
    });
  };

  render() {
    return (
      <div className="add-message-box">
        <AddMessageButton toggleForm={this.toggleForm} />
        <div
          className="add-message-form"
          style={{ display: this.state.displayForm ? "block" : "none" }}
        >
          <div className="triangle" />
          <span onClick={this.toggleForm} className="closeBtn">
            X
          </span>
          <AddMessageForm toggleForm={this.toggleForm} />
        </div>
      </div>
    );
  }
}

AddMessage.propTypes = {};

export default AddMessage;
