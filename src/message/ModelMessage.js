import PropTypes from "prop-types";

export const MessageStatuses = {
  VISIBLE: "visible",
  PRIVATE: "private"
};

export const IMessage = {
  id: PropTypes.string,
  creationDate: PropTypes.string,
  content: PropTypes.string,
  telegram: PropTypes.string,
  author: PropTypes.string,
  status: PropTypes.oneOf([MessageStatuses.VISIBLE, MessageStatuses.PRIVATE])
};
