import React from "react";
import { MessageStatuses, IMessage } from "./ModelMessage";
import WithLoading from "./../helpers/WithLoading";
import Message from "./Message";
import PropTypes from "prop-types";

const ListMessage = props => {
  return (
    <div className="row">
      <div className="col-12 right">
        {props.messages.map((message, index) => {
          if (message.status === MessageStatuses.VISIBLE) {
            return <Message key={index} id={index} message={message} />;
          }
          return null;
        })}
      </div>
    </div>
  );
};

const ListMessageWithLoading = WithLoading(ListMessage);

ListMessage.propTypes = {
  message: PropTypes.shape(IMessage)
};

export default ListMessageWithLoading;
