import React from "react";
import { playMorse } from "./../helpers/Morsify";
import moment from "moment";
import PropTypes from "prop-types";
import { IMessage } from "./ModelMessage";

const getMessageInfos = message => {
  return `By • ${message.author} • ${moment(message.creationDate).fromNow()}`;
};

const Message = props => {
  return (
    <div className={props.id === 0 ? `message fadein` : `message`}>
      <div className="bulle" onClick={() => playMorse(props.message.telegram)}>
        {props.message.telegram.split(" ").map((morseCode, index) => {
          return (
            <span key={index} className="mo">
              {morseCode}
            </span>
          );
        })}
        <div className="infos">{getMessageInfos(props.message)}</div>
      </div>
    </div>
  );
};

Message.propTypes = {
  id: PropTypes.number,
  message: PropTypes.shape(IMessage)
};

export default Message;
