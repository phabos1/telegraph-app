import React from "react";
import { shallow, mount } from "enzyme";
import ListMessage from "./ListMessage";
import Message from "./Message";
import * as moment from "moment";

it("should render ListMessage component", () => {
  let wrapper = shallow(<ListMessage messages={[]} />);
  expect(wrapper.props().messages).toEqual([]);

  wrapper = mount(<ListMessage messages={[]} />);
  wrapper.update();
  expect(wrapper.find(Message).length).toEqual(0);
});

it("should render 3 messages", () => {
  const messages = [
    {
      id: "1",
      status: "visible",
      telegram: "−−··",
      creationDate: "2019-06-20T22:31:58+02:00",
      content: "Hello",
      author: "user1"
    },
    {
      id: "2",
      status: "visible",
      telegram: "−−··",
      creationDate: "2019-06-20T22:31:58+02:00",
      content: "Hello",
      author: "user2"
    },
    {
      id: "3",
      status: "visible",
      telegram: "−−··",
      creationDate: "2019-06-20T22:31:58+02:00",
      content: "Hello",
      author: "user3"
    }
  ];
  let wrapper = shallow(<ListMessage messages={messages} />);
  expect(wrapper.props().messages.length).toEqual(3);
  expect(wrapper.props().messages).toEqual(messages);
  expect(wrapper.props().messages).toEqual(messages);

  wrapper = mount(<ListMessage messages={messages} />);
  wrapper.update();
  expect(wrapper.find(Message).length).toEqual(3);
});
