import { morsify } from "./Morsify";

it("should return morse code for string input", () => {
  let morsified = morsify("");
  expect(morsified).toEqual("");

  morsified = morsify(null);
  expect(morsified).toEqual("");

  morsified = morsify("Hello");
  expect(morsified).toEqual("···· · ·−·· ·−·· −−−");

  morsified = morsify("     Hello      ");
  expect(morsified).toEqual("···· · ·−·· ·−·· −−−");

  morsified = morsify("     Hel5432%`^^lo      ");
  expect(morsified).toEqual("···· · ·−·· ·−·· −−−");
});
