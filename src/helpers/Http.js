import axios from "axios";
import config from "./../config";
import toastr from "toastr";
import "toastr/build/toastr.min.css";

class Http {
  get(endpoint, callback) {
    axios
      .get(`${config.backendUrl}${endpoint}`)
      .then(response => callback(response))
      .catch(error => {
        toastr.error(`Something went really bad :(`);
      });
  }

  post(endpoint, payload, callback) {
    axios
      .post(`${config.backendUrl}${endpoint}`, payload)
      .then(response => callback(response))
      .catch(error => {
        toastr.error(`Something went really bad :(`);
      });
  }
}

export const http = new Http();
