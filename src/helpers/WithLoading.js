import React, { Component } from "react";
import Loading from "./../common/Loading";

const WithLoading = WrappedComponent => {
  return class WihLoadingComponent extends Component {
    render() {
      if (this.props.loading) return <Loading />;
      return <WrappedComponent {...this.props} />;
    }
  };
};

export default WithLoading;
