const morse = {
  A: "·−",
  B: "−···",
  C: "−·−·",
  D: "−··",
  E: "·",
  F: "··−·",
  G: "−−·",
  H: "····",
  I: "··",
  J: "·−−−",
  K: "−·−",
  L: "·−··",
  M: "−−",
  N: "−·",
  O: "−−−",
  P: "·−−·",
  Q: "−−·−",
  R: "·−·",
  S: "···",
  T: "−",
  U: "··−",
  V: "···−",
  W: "·−−",
  X: "−··−",
  Y: "−·−−",
  Z: "−−··"
};

export const morsify = message => {
  let morsified = "";
  if (message) {
    for (var i = 0; i < message.length; i++) {
      morsified += morse[message.charAt(i).toUpperCase()]
        ? `${morse[message.charAt(i).toUpperCase()]} `
        : "";
    }
  }
  return morsified.trim();
};

const getAudioContext = () => {
  if (window.hasOwnProperty("AudioContext")) {
    return window.AudioContext;
  }

  if (window.hasOwnProperty("webkitAudioContext")) {
    return window.webkitAudioContext;
  }

  return null;
};

const AudioContext = getAudioContext();

const context = AudioContext ? new AudioContext() : null;
const dot = 1.2 / 15;

export const playMorse = morseCodeMessage => {
  let oscillator = context.createOscillator();
  oscillator.type = "sine";
  oscillator.frequency.value = 600;

  let t = context.currentTime;
  let gainNode = context.createGain();
  gainNode.gain.setValueAtTime(0, t);

  if (morseCodeMessage) {
    for (var i = 0; i < morseCodeMessage.length; i++) {
      let letter = morseCodeMessage.charAt(i).trim();
      switch (letter) {
        case "·":
          gainNode.gain.setValueAtTime(1, t);
          t += dot;
          gainNode.gain.setValueAtTime(0, t);
          t += dot;
          break;
        case "−":
          gainNode.gain.setValueAtTime(1, t);
          t += 3 * dot;
          gainNode.gain.setValueAtTime(0, t);
          t += dot;
          break;
        case " ":
          t += 7 * dot;
          break;
        default:
          break;
      }
    }
  }

  oscillator.connect(gainNode);
  gainNode.connect(context.destination);

  oscillator.start();
};
