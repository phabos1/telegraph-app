import React from "react";
import { shallow } from "enzyme";
import App from "./App";
import Header from "./common/Header";
import Footer from "./common/Footer";
import Body from "./common/Body";

it("should render App component", () => {
  const wrapper = shallow(<App />);
  const header = <Header />;
  const body = <Body />;
  const footer = <Footer />;
  expect(wrapper.contains(header)).toEqual(true);
  expect(wrapper.contains(body)).toEqual(true);
  expect(wrapper.contains(footer)).toEqual(true);
});
