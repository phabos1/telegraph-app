import React from "react";
import AddMessage from "./../message/add/AddMessage";

const Header = () => {
  return (
    <div className="row">
      <div className="col-2" />
      <div className="col-8 center header">
        <header>Telegraph App</header>
        <p className="hidden-sm">A tiny app that help you send telegram</p>
        <p className="hidden-sm">
          Put on your headphone for better experience !
        </p>
      </div>
      <div className="col-2">
        <AddMessage />
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
