import React from "react";
import MessageContainer from "./../message/MessageContainer";

const Body = () => {
  return (
    <div className="row">
      <div className="col-12 center">
        <MessageContainer />
      </div>
    </div>
  );
};

Body.propTypes = {};

export default Body;
