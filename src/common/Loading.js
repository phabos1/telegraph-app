import React from "react";

const Loading = () => {
  return (
    <div className="lds-ring">
      <div />
      <div />
      <div />
      <div />
    </div>
  );
};

Loading.propTypes = {};

export default Loading;
