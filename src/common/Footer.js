import React from "react";

const Footer = () => {
  return (
    <div className="row">
      <div className="col-12 right">
        <footer>Made by @Phabos</footer>
      </div>
    </div>
  );
};

Footer.propTypes = {};

export default Footer;
