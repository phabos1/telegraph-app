import React from "react";
import Header from "./common/Header";
import Body from "./common/Body";
import Footer from "./common/Footer";

const App = () => {
  return (
    <div className="container">
      <Header />
      <Body />
      <Footer />
    </div>
  );
};

App.propTypes = {};

export default App;
